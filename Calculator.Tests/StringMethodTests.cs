﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Calculator.StringMethod.Tests
{
    [TestFixture, Description("Test fixture for  StringMethod")]
    public class StringMethodTests
    {
        StringMethod s;

        [SetUp, Order(1), Description("Setup for begining of tests")]
        public void Init()
        {
            s = new StringMethod();
        }

        [TearDown, Description("Setup for ending of tests")]
        public void CleanUp()
        {
            s = null;
        }

        [Test]
        public void StringAdd_Str1andStr2_Str1PlusStr2retorn()
        {
            String str1 = "Hello, ";
            String str2 = "world!";
            string expected = s.AddString(str1, str2);
            string actual = str1 + str2;
            Assert.IsNotEmpty(str1);
            Assert.IsNotEmpty(str2);
            Assert.AreEqual(expected, actual, "String add method pass");
        }

        [Test]
        public void StringAdd_Str1andStr2_Str1Contains()
        {
            String str1 = "Hello, ";
            String str2 = "world!";
            string expected = s.AddString(str1, str2);
            string actual = str1 + str2;
            Assert.That(expected, Contains.Substring(str1));
        }

        [Test]
        public void StringAdd_IgnoringCase()
        {
            String str1 = "Hello, ";
            String str2 = "world!";
            string expected = s.AddString(str1, str2);
            string actual = "HELLO, WORLD!";
            StringAssert.AreEqualIgnoringCase(actual, expected);
        }

        [Test]
        public void StringAdd_Ignoringse()
        {
            String str1 = "Hello, ";
            String str2 = "world!";
            string expected = s.AddString(str1, str2);
            string actual = str1 + str2;
            StringAssert.StartsWith("H", actual);
        }
    }
}