﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Calculator.Tests
{
    [SetUpFixture, Description("SetUp fixture for Calclator")]
    public class CalculatorSetUp
    {
        public Calculator c;

        [OneTimeSetUp, Description("Setup for begining of tests")]
        public void Init()
        {
            c = new Calculator();
        }

        [OneTimeTearDown, Description("Setup for ending of tests")]
        public void CleanUp()
        {
            c = null;
        }
    }

    [TestFixture, Description("Test fixture for Calclator")]
    public class CalculatorTests
    {
        public Calculator c = new Calculator();

        [TestCase(2, -2, ExpectedResult = 0)]
        public double Sum_2and_minus2_0return(double x, double y)
        {
            double actual = c.Add(x, y);
            return actual;
        }

        [TestCase(10, 10, ExpectedResult = 0)]
        [TestCase(200, 10, ExpectedResult = 190)]
        [TestCase(0, 10, ExpectedResult = -10)]

        public double Sub(double x, double y)
        {
            double actual = c.Sub(x, y);
            return actual;
        }

        [TestCase(10, 10, 100)]
        [TestCase(200, 10, 2000)]
        [TestCase(0, 10, 0)]
        public void Mul(double x, double y, double expected)
        {
            double actual = c.Mul(x, y);
            Assert.That(expected, Is.EqualTo(actual));
        }

        [Test, Order(2)]
        public void Div_10and1_10return()
        {
            double x = 10;
            double y = 1;
            double expected = 10;
            double actual = c.Div(x, y);
            Assert.That(expected, Is.EqualTo(actual));
        }

        [Test, Ignore("10 / 10should be equal to NaN")]
        public void Fail_Ignor_Div_10and0_NaNreturn()
        {
            double x = 10;
            double y = 0;
            double actual = c.Div(x, y);
            Assert.IsNaN(actual, $"{x} / {y} should be equal to NaN");
        }

        [Test, Retry(2)]
        public void Div_10and0_not0return()
        {
            double x = 10;
            double y = 0;
            double actual = c.Div(x, y);
            Assert.IsNotNull(actual, $"{x} / {y} should be equal to 0");
        }
    }
}
