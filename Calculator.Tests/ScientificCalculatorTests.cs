﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Calculator.ScientificCalculator.Tests
{
    [TestFixture, Description("Test fixture for ScientificCalculator")]
    public class ScientificCalculatorTests
    {
        ScientificCalculator c;

        [SetUp, Order(1), Description("Setup for begining of tests")]
        public void Init()
        {
            c = new ScientificCalculator();
        }

        [TearDown, Description("Setup for ending of tests")]
        public void CleanUp()
        {
            c = null;
        }

        [Test, Order(3)]
        public void Pow_2and0_1return()
        {
            double x = 2;
            double y = 0;
            double expected = Math.Pow(x, y);
            double actual = c.Pow(x, y);
            Assert.AreEqual(expected, actual, $"{x} ^ {y} should be equal to {expected}");
        }

        [Test, Repeat(0)]
        public void Pow_2and3_8return()
        {
            double x = 2;
            double y = 3;
            double expected = Math.Pow(x, y);
            double actual = c.Pow(x, y);
            Assert.AreEqual(expected, actual, $"{x} pow {y} should be equal to {expected}");
        }

        [Test]
        public void Mod_15and6_3return()
        {
            double x = 15;
            double y = 6;
            double expected = 3;
            double actual = c.Mod(x, y);
            Assert.AreEqual(expected, actual, $"{x} mod {y} should be equal to {expected}");
        }

        [Test]
        public void ArraySum_15and6and4and6_31return()
        {
            double[] arr = new double[] { 15, 6, 4, 6 };
            double expected = arr.Sum();
            double actual = c.ArraySum(arr);
            Assert.AreEqual(expected, actual, $"Sum of 15  6  4  6  should be equal to {expected}");
        }

        [Test]
        public void ArrayMin_15and6and4and4_4return()
        {
            double[] arr = new double[] { 15, 6, 4, 4 };
            double expected = arr.Min();
            double actual = c.ArrayMin(arr);
            Assert.AreEqual(expected, actual, $"Min of 15  6  4  6  should be equal to {expected}");
            Assert.Ignore("ArrayMin_15and6and4and4_4return");
        }

        [Test]
        public void ArrayMax_15and6and4and15_15return()
        {
            double[] arr = new double[] { 3, 6, 4, 15 };
            double expected = arr.Max();
            double actual = c.ArrayMax(arr);
            Assert.AreEqual(expected, actual, $"Sum of 3  6  4  6  should be equal to {expected}");
            Assert.Pass("ArrayMax_15and6and4and15_15return");

        }

        [Test]
        public void Sqrt121_11return()
        {
            double x = 121;
            double expected = c.Sqrt(x);
            double actual = Math.Sqrt(x);
            Assert.AreEqual(expected, actual, "Sqrt " + x + " should be equal to " + expected);
            Assert.Fail("Sqrt121_11return");
        }
    }
}