﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.StringMethod
{
    public class StringMethod: Calculator 
    {
        public string AddString(string str1, string str2)
        {
            return str1 + str2;
        }
    }
}
