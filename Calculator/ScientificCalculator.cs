﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.ScientificCalculator
{
    public class ScientificCalculator : Calculator
    {
        public double Pow(double x, double y)
        {
            double pow = 1;
            for (int i = 1; i < y + 1 ; i++) pow = Mul(pow,  x);
            return pow;
        }

        public double Mod(double x, double y)
        {
            return x % y;
        }

        public double ArraySum(double[] arr)
        {
            double sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum = Add(sum, arr[i]);
            }
            return sum;
        }

        public double ArrayMin(double[] arr)
        {
            double min = arr[0], minIndex = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (min > arr[i])
                {
                    min = arr[i];
                    minIndex = i;
                }
            }
            return min;
        }

        public double ArrayMax(double[] arr)
        {
            double max = arr[0], maxIndex = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            return max;
        }

        public double Sqrt(double value)
        {
            double root = 1;
            int i = 0;
            while(true)
            {
                i = i + 1;
                root = Div((Div(value, Add(root, root))), 2);
                if(i == Add(value, 1)) { break; }
            }
            return root;
        }
    }
}
